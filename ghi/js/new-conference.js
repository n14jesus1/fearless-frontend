window.addEventListener("DOMContentLoaded", async () => {
    const locationsUrl = 'http://localhost:8000/api/locations/';
    try {
        const locationsResponse = await fetch(locationsUrl);
        if (locationsResponse.ok) {
            const locationsData = await locationsResponse.json();
            const locations = locationsData.locations

            for (let location of locations) {
                const locationSelect = document.getElementById("location");
                const option = document.createElement("option");
                option.value = location.id;
                option.innerHTML = location.name;
                locationSelect.appendChild(option);
            }

            const formTag = document.getElementById("create-conference-form");
            formTag.addEventListener("submit", async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));

                const conferenceUrl = 'http://localhost:8000/api/conferences/'
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(conferenceUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newConference = await response.json();
                } else {
                    throw Error(`Could not complete post request to ${conferenceUrl}`);
                }
            });
        } else {
            throw Error(`No data response from ${locationsUrl}`);
        }
    }
    catch(e) {
        console.error(e)
    }
});
