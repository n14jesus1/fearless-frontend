

window.addEventListener("DOMContentLoaded", async () => {
    const statesUrl = "http://localhost:8000/api/states/";

    try {
        const statesResponse = await fetch(statesUrl);
        if (statesResponse.ok) {
            const stateData = await statesResponse.json();
            const states = stateData.states

            for (let state of states) {
                const stateSelect = document.getElementById("state");
                const option = document.createElement("option");
                option.value = state.abbreviation;
                option.innerHTML = state.name;
                stateSelect.appendChild(option);
            }

            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag)
                const json = JSON.stringify(Object.fromEntries(formData));

                const locationUrl = 'http://localhost:8000/api/locations/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(locationUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newLocation = await response.json();
                } else {
                    throw Error(`Could not complete post request to ${locationUrl}`);
                }
            });
        } else {
            throw Error(`No data response from ${statesUrl}`);
        }
    }
    catch(e) {
        console.error(e);
    }
});
