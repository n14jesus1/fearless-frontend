function createCard(name, description, pictureUrl, location, startDate, endDate) {
    return `
        <div class="col">
            <div class="card shadow">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle text-secondary">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                    ${startDate} - ${endDate}
                </div>
            </div>
        </div>
    `
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw Error(response.error());
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name;
                    const starts = new Date(details.conference.starts);
                    const ends = new Date(details.conference.ends);
                    const startDate = starts.toLocaleDateString();
                    const endDate = ends.toLocaleDateString();
                    const html = createCard(title, description, pictureUrl, location, startDate, endDate);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;

                }
            }

        }
    } catch (e) {
        const message = `
            <div class="alert alert-danger" role="alert">
                Error: Could not find data at ${url}
            </div>
        `;
            const container = document.querySelector('.container');
            container.innerHTML += message;
            console.error(e);
    }

});
